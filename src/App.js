import React, {useEffect, useState} from 'react';
import './App.css';
import FormControl from "@material-ui/core/FormControl/FormControl";
import Select from "@material-ui/core/Select/Select";
import MenuItem from "@material-ui/core/MenuItem/MenuItem";
import InputLabel from "@material-ui/core/InputLabel/InputLabel";
import InfoBox from './InfoBox';
import { Card, CardContent } from '@material-ui/core';
import Table from './Table' ;
import {sortData} from "./util";

function App() {
    // STATE = How to write a variable in React
    // The state countries, setCountry hold the country value from the API
    // and is used to set the value in the dropdows
    const [countries, setCountry] = useState([]);

    // The state dropdownCountry, setdropdownCountry hold the default value
    // from the <Select variant={"outlined"} value={dropdownCountry}>
    const[dropdownCountry, setdropdownCountry] = useState(["worldwide"]);

    // The state dropdownCountry, setdropdownCountry hold the default value
    // from the <Select variant={"outlined"} value={dropdownCountry}>
    const[countryInfo, setcountryInfo] = useState({});

    const[tableData, setTableData] = useState([]);

    // USEEFFECT = Runs a piece of code based on a given condition
    useEffect(() => {
    // async = send a request , wait to the response and does something with it

     // This Function GET the endpoint , runs the map function on the response
     // collects the JSON value in const countries under keys name and value
     // passes the value to state setCountry
     // Since function is async , getCountriesData gets called
        const getCountriesData = async () => {
            await fetch ("https://disease.sh/v3/covid-19/countries")
                .then((response) => response.json())
                .then((data) => {
                   const countries = data.map((country) => (
                       {
                           name: country.country,
                           value: country.countryInfo.iso2
                       }
                   ));
                   const sortedData = sortData(data);
                   setTableData(sortedData);
                   setCountry(countries)
                })
        }
        getCountriesData();
    }, [])

    useEffect(() => {
                 fetch ("https://disease.sh/v3/covid-19/all ")
                .then((response) => response.json())
                .then((data) => {
                    setcountryInfo(data);
                })
    },[])

    // THIS FUNCTION stores the value the user selects from the dropdown
    // and then sets the value in the dropdown UI
    const onCountryChange = async(event) => {
        const countryChange = event.target.value;
        setdropdownCountry(countryChange);

        const url = 
            countryChange === "worldwide" 
                ? "https://disease.sh/v3/covid-19/all"
                : `https://disease.sh/v3/covid-19/countries/${countryChange}`;
         
        await fetch(url)
        .then(response => response.json())
        .then(data => {
            setdropdownCountry(countryChange);
            setcountryInfo(data);
        }) ;
    } ;
    
  return (
    <div className="App">
      <div className="app_left">
        <div className={"app_header"}>
            <h1>COVID-19 TRACKER</h1>
            <FormControl className="app_dropdown">
                <InputLabel shrink id="demo-simple-select-disabled-label">Country</InputLabel>
                <Select variant={"outlined"} onChange={onCountryChange} value={dropdownCountry}>
                    <MenuItem value="worldwide">Worldwide</MenuItem>
                    {countries.map((country) => (
                            <MenuItem value={country.value}>{country.name}</MenuItem>
                    ))}
                </Select>
            </FormControl>
        </div>
        <div className="app_stats">
            <InfoBox title="Coronovirus Cases" cases={countryInfo.todayCases} total={countryInfo.cases}></InfoBox>
            <InfoBox title="Recovered" cases={countryInfo.todayRecovered} total={countryInfo.recovered}></InfoBox>
            <InfoBox title="Deaths" cases={countryInfo.todayDeaths} total={countryInfo.deaths}></InfoBox>
        </div>
        </div>

        <Card className="app_right">
            <CardContent>
                <h3> Live Cases By Country</h3>
                <Table countries={tableData}></Table>
                    <h3>Worldwide New Cases</h3>

            </CardContent>
        </Card>


    </div>
  );
}

export default App;
